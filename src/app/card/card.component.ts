import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { TrelloAPIService } from '../trello-api.service';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent {
  constructor(private trelloApiService: TrelloAPIService) {}
  @Input() listId: string | null = null;
  @Input() cards: any[] = [];
  selectedCardId: string | null = null;
  selectedCardChecklists: any[] = [];
  isModalOpen: boolean = false;
  isAddingCard: boolean = false;
  newCardName: string = '';

  addCard() {
    if (this.newCardName.trim() !== '') {
      this.trelloApiService
        .createCard(this.listId!, this.newCardName)
        .subscribe(
          (response: any) => {
            console.log('Card created', response);
            this.newCardName = '';
            this.cards.push(response); // Add the newly created card to the cards array
            this.isAddingCard = false;
          },
          (error: any) => {
            console.log('Error creating card', error);
          }
        );
    }
  }

  cancelAddingCard() {
    this.isAddingCard = false;
    this.newCardName = '';
  }

  showCardForm() {
    this.isAddingCard = true;
  }
  deleteCard(cardId: string) {
    this.trelloApiService.deleteCard(cardId).subscribe(
      () => {
        console.log('Card deleted successfully');
        // Remove the deleted card from the cards array
        this.cards = this.cards.filter((card) => card.id !== cardId);
      },
      (error: any) => {
        console.error('Error deleting card:', error);
      }
    );
  }
  openChecklistModal(cardId: string) {
    this.selectedCardId = cardId;
    this.fetchChecklistsForCard(cardId);
    this.isModalOpen = true;
  }
  fetchChecklistsForCard(cardId: string) {
    this.trelloApiService.getChecklistsForCard(cardId).subscribe(
      (response: any) => {
        this.selectedCardChecklists = response;
        console.log(response, 'checklists');
      },
      (error: any) => {
        console.error('Error fetching checklists:', error);
      }
    );
  }
  closeChecklistModal() {
    this.isModalOpen = false;
  }
}
