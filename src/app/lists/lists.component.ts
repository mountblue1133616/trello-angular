import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TrelloAPIService } from '../trello-api.service';
@Component({
  selector: 'app-lists',
  templateUrl: './lists.component.html',
  styleUrls: ['./lists.component.css'],
})
export class ListsComponent {
  constructor(
    private trelloApiService: TrelloAPIService,
    private route: ActivatedRoute
  ) {}

  boardList: any[] = [];
  boardId: string = '';
  newListName: string = '';
  cardsMap: { [listId: string]: any } = {};

  ngOnInit() {
    this.route.params.subscribe((params) => {
      this.boardId = params[`boardId`];
      this.fetchBoardLists();
    });
  }

  fetchBoardLists() {
    this.trelloApiService.getBoardsList(this.boardId).subscribe(
      (response: any) => {
        this.boardList = response;
        this.boardList.forEach((list) => {
          this.fetchCardsForList(list.id);
        });
      },
      (error: any) => {
        console.error('Error fetching board lists:', error);
      }
    );
  }

  fetchCardsForList(listId: string) {
    this.trelloApiService.getCardsForList(listId).subscribe(
      (response: any) => {
        this.cardsMap[listId] = response;
      },
      (error: any) => {
        console.error('Error fetching cards:', error);
      }
    );
  }
  createList() {
    if (this.newListName.trim() !== '') {
      this.trelloApiService
        .createList(this.boardId, this.newListName)
        .subscribe((response: any) => {
          console.log('List created', response);
          this.newListName = '';
          this.fetchBoardLists();
        }),
        (error: any) => {
          console.log('Error creating list', error);
        };
    }
  }

  deleteList(listId: string) {
    this.trelloApiService.deleteList(listId).subscribe(
      () => {
        console.log('List deleted successfully');
        // Remove the deleted list from the boardList array
        this.boardList = this.boardList.filter((list) => list.id !== listId);
        // Remove the deleted list's cards from the cardsMap
        delete this.cardsMap[listId];
      },
      (error: any) => {
        console.error('Error deleting list:', error);
      }
    );
  }
}
