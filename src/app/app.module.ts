import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BoardsComponent } from './boards/boards.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ListsComponent } from './lists/lists.component';
import { CardComponent } from './card/card.component';
import { ChecklistsComponent } from './checklists/checklists.component';
// import { CheckitemsComponent } from './checkitems/checkitems.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
// import { CardModalComponent } from './card-modal/card-modal.component';
import { ChecklistitemComponent } from './checklistitem/checklistitem.component';

@NgModule({
  declarations: [
    AppComponent,
    BoardsComponent,
    ListsComponent,
    CardComponent,
    ChecklistsComponent,
    ChecklistitemComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatDialogModule,
  ],

  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
