import { Component, OnInit } from '@angular/core';
import { TrelloAPIService } from '../trello-api.service';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.css'],
})
export class BoardsComponent implements OnInit {
  boards: any[] = [];
  newBoardName: string = '';

  constructor(private trelloApiService: TrelloAPIService) {}

  ngOnInit() {
    this.getAllBoards();
  }

  getAllBoards() {
    const observer = {
      next: (response: any) => {
        this.boards = response;
        console.log(this.boards, 'boards');
      },
      error: (error: any) => {
        console.log('Error fetching boards', error);
      },
    };

    this.trelloApiService.getAllBoards().subscribe(observer);
  }

  createBoard() {
    if (this.newBoardName.trim() !== '') {
      this.trelloApiService
        .createBoard(this.newBoardName)
        .subscribe((response: any) => {
          console.log('Board created', response);
          this.newBoardName = ''; //again making the ui clear
          this.getAllBoards();
        }),
        (error: any) => {
          console.log('Error creating board', error);
        };
    }
  }
}
