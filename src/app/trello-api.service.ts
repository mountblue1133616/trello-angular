import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class TrelloAPIService {
  private key = '0d93fc7aa427abbf91bcaa4357ecdb93';
  private token =
    'ATTA1017451fbb7dfb4a5de5e13623ba8b5ea0cb5815471ba54995a07bbb0613038a28A503EA';
  constructor(private http: HttpClient) {}

  getAllBoards(): Observable<any> {
    const allBoardsUrl = `https://api.trello.com/1/members/me/boards?key=${this.key}&token=${this.token}`;
    return this.http.get(allBoardsUrl);
  }

  createBoard(boardName: string): Observable<any> {
    const createBoardUrl = `https://api.trello.com/1/boards?key=${this.key}&token=${this.token}&name=${boardName}`;
    return this.http.post(createBoardUrl, null);
  }

  getBoardsList(boardId: string): Observable<any> {
    const boardListsUrl = `https://api.trello.com/1/boards/${boardId}/lists?key=${this.key}&token=${this.token}`;
    return this.http.get(boardListsUrl);
  }
  createList(boardId: string, listName: string): Observable<any> {
    const createListUrl = `https://api.trello.com/1/lists?key=${this.key}&token=${this.token}&name=${listName}&idBoard=${boardId}`;
    return this.http.post(createListUrl, null);
  }
  deleteList(listId: string): Observable<any> {
    const deleteListUrl = `https://api.trello.com/1/lists/${listId}?key=${this.key}&token=${this.token}`;
    const requestBody = {
      closed: true,
    };
    return this.http.put(deleteListUrl, requestBody);
  }

  getCardsForList(listId: string): Observable<any> {
    const listCardsUrl = `https://api.trello.com/1/lists/${listId}/cards?key=${this.key}&token=${this.token}`;
    return this.http.get(listCardsUrl);
  }

  createCard(listId: string, cardName: string): Observable<any> {
    const createCardUrl = `https://api.trello.com/1/cards?key=${this.key}&token=${this.token}&idList=${listId}&name=${cardName}`;
    return this.http.post(createCardUrl, null);
  }
  deleteCard(cardId: string): Observable<any> {
    const deleteCardUrl = `https://api.trello.com/1/cards/${cardId}?key=${this.key}&token=${this.token}`;
    return this.http.delete(deleteCardUrl);
  }
  getChecklistsForCard(cardId: string): Observable<any> {
    const checklistsUrl = `https://api.trello.com/1/cards/${cardId}/checklists?key=${this.key}&token=${this.token}`;
    return this.http.get(checklistsUrl);
  }
  createChecklist(cardId: string, checklistName: string): Observable<any> {
    const createChecklistUrl = `https://api.trello.com/1/checklists?key=${this.key}&token=${this.token}&idCard=${cardId}&name=${checklistName}`;
    return this.http.post(createChecklistUrl, null);
  }
  deleteChecklist(checklistId: string): Observable<any> {
    const deleteChecklistUrl = `https://api.trello.com/1/checklists/${checklistId}?key=${this.key}&token=${this.token}`;
    return this.http.delete(deleteChecklistUrl);
  }
  getCheckItemsForChecklist(checklistId: string): Observable<any> {
    const url = `https://api.trello.com/1/checklists/${checklistId}/checkItems?key=${this.key}&token=${this.token}`;
    return this.http.get(url);
  }
  toggleCheckItem(checklistId: string, checkItemId: string): Observable<any> {
    const toggleUrl = `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${this.key}&token=${this.token}`;
    return this.http.put(toggleUrl, {});
  }

  deleteCheckitem(checklistId: string, checkItemId: string): Observable<any> {
    const deleteCheckItemUrl = `https://api.trello.com/1/checklists/${checklistId}/checkItems/${checkItemId}?key=${this.key}&token=${this.token}`;
    return this.http.delete(deleteCheckItemUrl);
  }
}
