import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { TrelloAPIService } from '../trello-api.service';

@Component({
  selector: 'app-checklistitem',
  templateUrl: './checklistitem.component.html',
  styleUrls: ['./checklistitem.component.css'],
})
export class ChecklistitemComponent {
  @Input() checkListId: string | null = null;
  @Input() checkItem: any;

  constructor(private trelloApiService: TrelloAPIService) {}

  toggleCheckItem() {
    this.checkItem.state =
      this.checkItem.state === 'complete' ? 'incomplete' : 'complete';
    this.trelloApiService
      .toggleCheckItem(this.checkListId!, this.checkItem.id)
      .subscribe(
        () => {
          console.log('Check item toggled successfully');
        },
        (error: any) => {
          console.error('Error toggling check item:', error);
        }
      );
  }

  deleteCheckItem() {
    this.trelloApiService
      .deleteCheckitem(this.checkListId!, this.checkItem.id)
      .subscribe(
        () => {
          console.log('Check item deleted successfully');
        },
        (error: any) => {
          console.error('Error deleting check item:', error);
        }
      );
  }
}
