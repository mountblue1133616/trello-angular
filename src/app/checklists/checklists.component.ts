import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { TrelloAPIService } from '../trello-api.service';

@Component({
  selector: 'app-checklists',
  templateUrl: './checklists.component.html',
  styleUrls: ['./checklists.component.css'],
})
export class ChecklistsComponent {
  constructor(private trelloApiService: TrelloAPIService) {}
  @Input() cardId: string | null = null;
  @Input() checklists: any[] = [];
  checkItems: any[] = [];
  newChecklistName: string = '';
  isAddingChecklist: boolean = false;
  @Input() checklistId: string | null = null;

  addChecklist(checklistName: string) {
    if (checklistName.trim() !== '') {
      this.trelloApiService
        .createChecklist(this.cardId!, checklistName)
        .subscribe(
          (response: any) => {
            console.log('Checklist created', response);
            // Add the newly created checklist to the selected card's checklists
            this.checklists.push(response);
          },
          (error: any) => {
            console.log('Error creating checklist', error);
          }
        );
    }
  }
  cancelAddingChecklist() {
    this.isAddingChecklist = false;
    this.newChecklistName = '';
  }
  showChecklistForm() {
    this.isAddingChecklist = true;
  }
  deleteCheckList(checklistId: string) {
    this.trelloApiService.deleteChecklist(checklistId).subscribe(
      () => {
        console.log('Checklist deleted successfully');

        this.checklists = this.checklists.filter(
          (checklist) => checklist.id !== checklistId
        );
      },
      (error: any) => {
        console.error('Error deleting card:', error);
      }
    );
  }
  fetchCheckItemsForChecklist() {
    this.trelloApiService
      .getCheckItemsForChecklist(this.checklistId!)
      .subscribe(
        (response: any) => {
          this.checkItems = response;
        },
        (error: any) => {
          console.error('Error fetching check items:', error);
        }
      );
  }
  // toggleCheckItem(checkItemId: string) {
  //   // Call the service method to toggle the check item
  //   this.trelloApiService
  //     .toggleCheckItem(this.checklistId!, checkItemId)
  //     .subscribe(
  //       (response: any) => {
  //         // Update the state of the check item in the array
  //         const updatedCheckItem = this.checkItems.find(
  //           (item) => item.id === checkItemId
  //         );
  //         if (updatedCheckItem) {
  //           updatedCheckItem.state = response.state;
  //         }
  //       },
  //       (error: any) => {
  //         console.error('Error toggling check item:', error);
  //       }
  //     );
  // }

  // deleteCheckitem(checkItemId: string) {
  //   // Call the service method to delete the check item
  //   this.trelloApiService
  //     .deleteCheckitem(this.checklistId!, checkItemId)
  //     .subscribe(
  //       () => {
  //         // Remove the deleted check item from the array
  //         this.checkItems = this.checkItems.filter(
  //           (item) => item.id !== checkItemId
  //         );
  //       },
  //       (error: any) => {
  //         console.error('Error deleting check item:', error);
  //       }
  //     );
  // }
}
